import React, { Component } from 'react';
import { Dimensions, Platform } from 'react-native';
import { StackNavigator, createStackNavigator, createBottomTabNavigator, withNavigation } from 'react-navigation';
import { Icon } from 'react-native-elements';

import Login from './screens/Login';
import Register from './screens/Register';

import Dashboard from './routesDashboard';

let screens = Dimensions.get('window');

//tab yang ditampilan dibawah
export const Tabs = createBottomTabNavigator({
	'Login': {
		screen: Login,
		navigationOptions: {
			tabBarLabel: 'Login',
			tabBarIcon: ({ tintColor }) => <Icon name="login" type="entypo" size={28} color={tintColor} />
		}
	},
	'Register': {
		screen: Register,
		navigationOptions: {
			tabBarLabel: 'Register',
			tabBarIcon: ({ tintColor }) => <Icon name="add-user" type="entypo" size={28} color={tintColor} />
		}
	}
});

// export const LoginStack = createStackNavigator({
// 	Login: {
// 		screen: Login,
// 		navigationOptions: ({navigation}) => ({
// 			header: null
// 		})
// 	}
// });

//bikin constant untuk bisa di panggil constannya aja di App.js
export const Index = () => {
	return createStackNavigator(
		{
			Tabs: {
				screen: Tabs,
				navigationOptions: ({navigation}) => ({
					gesturesEnabled: false
				})
			},
			Login: {
				screen: Login,
				navigationOptions: ({navigation}) => ({
					gesturesEnabled: false
				})
			},
			Dashboard: { screen: Dashboard }
		},
		{
			headerMode: "none",
			mode: "modal"
		}
	);
};

//bikin routing default agar bisa dipanggir dr mana saja
export default createStackNavigator(
	{
		Tabs: {
			screen: Tabs,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		},
		Login: {
			screen: Login,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		}
	},
	{
		headerMode: "none",
		mode: "modal"
	}
);
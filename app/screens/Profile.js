import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  ScrollView
} from 'react-native';

export default class Profile extends Component {

  constructor(props) {
    super(props);
  
    this.state = {};

    //get session multiple
    AsyncStorage.multiGet(['id', 'name', 'class', 'phone', 'email', 'password', 'photo', 'bio']).then((data) => {
        let email = data[4][1];
        let password = data[5][1];

        if (email != null){
          //console.log('ada session');
        } else {
          //kalo ga ada session redirect ke login page/routes.js
          this.props.navigation.navigate('Login');
        }
    });

    //get session single
    AsyncStorage.getItem('id', (error, result) => {
        if (result) {
            this.setState({
                id: result
            });
        }
    });

    AsyncStorage.getItem('name', (error, result) => {
        if (result) {
            this.setState({
                name: result
            });
        }
    });

    AsyncStorage.getItem('class', (error, result) => {
        if (result) {
            this.setState({
                class: result
            });
        }
    });

    AsyncStorage.getItem('phone', (error, result) => {
        if (result) {
            this.setState({
                phone: result
            });
        }
    });

    AsyncStorage.getItem('email', (error, result) => {
        if (result) {
            this.setState({
                email: result
            });
        }
    });

    AsyncStorage.getItem('password', (error, result) => {
        if (result) {
            this.setState({
                password: result
            });
        }
    });

    AsyncStorage.getItem('photo', (error, result) => {
        if (result) {
            this.setState({
                photo: result
            });
        }
    });

    AsyncStorage.getItem('bio', (error, result) => {
        if (result) {
            this.setState({
                bio: result
            });
        }
    });
  }

  //bikin function logout
  UserLogoutFunction = () => {
    AsyncStorage.clear(); // to clear the token 
    this.props.navigation.navigate('Login', {
      onGoBack: () => this.refresh(),
    });
  }

  UserEditProfile = (student_id, student_name, student_class, student_phone, student_email, student_password, student_bio) => {
    //console.log('masuk');
    this.props.navigation.navigate('EditProfile', {
      ID: student_id,
      NAME: student_name,
      CLASS: student_class,
      PHONE: student_phone,
      EMAIL: student_email,
      PASSWORD: student_password,
      BIO: student_bio
    });
  }

  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
          <View style={styles.header}></View>
          <Image style={styles.avatar} source={{uri: 'http://dev.bymankind.com/reactnative-api-crud/uploads/'+this.state.photo}}/>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>{this.state.name}</Text>
              <Text style={styles.info}>{this.state.class}</Text>
              <Text style={styles.description}>{this.state.bio}</Text>
              
              <TouchableOpacity style={styles.buttonContainer} 
                onPress={this.UserEditProfile.bind(
                    this, this.state.id,
                    this.state.name,
                    this.state.class,
                    this.state.phone,
                    this.state.email,
                    this.state.password,
                    this.state.bio
                  )}
              >
                <Text>Edit Profile</Text>  
              </TouchableOpacity> 
              <TouchableOpacity style={styles.buttonContainer} onPress={this.UserLogoutFunction}>
                <Text>Logout</Text>  
              </TouchableOpacity>              
              {/*<TouchableOpacity style={styles.buttonContainer}>
                <Text>Opcion 2</Text> 
              </TouchableOpacity>*/}
            </View>
        </View>
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#00BFFF",
    height:200,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
    position: 'absolute',
    marginTop:130
  },
  name:{
    fontSize:22,
    color:"#FFFFFF",
    fontWeight:'600',
  },
  body:{
    marginTop:40,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:30,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#00BFFF",
    marginTop:10
  },
  description:{
    fontSize:16,
    color: "#696969",
    marginTop:10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop:10,
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
});
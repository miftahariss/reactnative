import React, { Component } from 'react';
import { StyleSheet, TextInput, View, Alert, TouchableOpacity, Text, AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class EditProfile extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	TextInput_Student_ID: '',
	  	TextInput_Student_Name: '',
	  	TextInput_Student_Class: '',
	  	TextInput_Student_PhoneNumber: '',
	  	TextInput_Student_Email: '',
	  	TextInput_Student_Password: '',
      TextInput_Student_Bio: ''
	  };
	}

	componentDidMount(){
		//Received Student Details Sent From Previous Activity and Set Into State
		this.setState({
			TextInput_Student_ID: this.props.navigation.state.params.ID,
		  	TextInput_Student_Name: this.props.navigation.state.params.NAME,
		  	TextInput_Student_Class: this.props.navigation.state.params.CLASS,
		  	TextInput_Student_PhoneNumber: this.props.navigation.state.params.PHONE,
		  	TextInput_Student_Email: this.props.navigation.state.params.EMAIL,
		  	TextInput_Student_Password: this.props.navigation.state.params.PASSWORD,
        TextInput_Student_Bio: this.props.navigation.state.params.BIO
		})
	}

	UserUpdateFunction = () => {
		fetch('http://dev.bymankind.com/reactnative-api-crud/UpdateStudentRecord.php', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				student_id : this.state.TextInput_Student_ID,
                student_name : this.state.TextInput_Student_Name,
                student_class : this.state.TextInput_Student_Class,
                student_phone_number : this.state.TextInput_Student_PhoneNumber,
                student_email : this.state.TextInput_Student_Email,
                student_password : this.state.TextInput_Student_Password,
                student_bio: this.state.TextInput_Student_Bio
			})
		}).then((response) => response.json())
			.then((responseJson) => {
				if(responseJson != '404'){
					//replace session dengan data yang baru diupdate
					AsyncStorage.multiSet([
		                ["id", this.state.TextInput_Student_ID],
		                ["name", this.state.TextInput_Student_Name],
		                ["class", this.state.TextInput_Student_Class],
		                ["phone", this.state.TextInput_Student_PhoneNumber],
		                ["email", this.state.TextInput_Student_Email],
		                ["password", this.state.TextInput_Student_Password],
                    ["bio", this.state.TextInput_Student_Bio]
		            ]);

					Alert.alert('Profile berhasil diubah');

					//redirect
            		this.props.navigation.navigate('Tabs');
				} else {
					Alert.alert('Profile gagal diubah');
				}
			}).catch((error) => {
				console.error(error);
			});
	}

	render() {
    return (
      <View style={styles.MainContainer}>
        <Text style={ styles.TextComponentStyle }>Student Register Form</Text>

        <TextInput
          placeholder="Enter User Name"

          value={this.state.TextInput_Student_Name}

          onChangeText={TextInputValue => this.setState({TextInput_Student_Name : TextInputValue})}

          underlineColorAndroid='transparent'

          style={styles.TextInputStyleClass}
        />

        <TextInput
          placeholder="Enter User Class"

          value={this.state.TextInput_Student_Class}

          onChangeText={TextInputValue => this.setState({TextInput_Student_Class : TextInputValue})}

          underlineColorAndroid='transparent'

          style={styles.TextInputStyleClass}
        />

        <TextInput
          placeholder="Enter User Phone Number"

          value={this.state.TextInput_Student_PhoneNumber}

          onChangeText={TextInputValue => this.setState({TextInput_Student_PhoneNumber : TextInputValue})}

          underlineColorAndroid='transparent'

          style={styles.TextInputStyleClass}
        />

        <TextInput
          placeholder="Enter User Email"

          value={this.state.TextInput_Student_Email}

          onChangeText={TextInputValue => this.setState({TextInput_Student_Email : TextInputValue})}

          underlineColorAndroid='transparent'

          style={styles.TextInputStyleClass}
        />

        <TextInput
          placeholder="Enter User Password"

          value={this.state.TextInput_Student_Password}

          onChangeText={TextInputValue => this.setState({TextInput_Student_Password : TextInputValue})}

          underlineColorAndroid='transparent'

          style={styles.TextInputStyleClass}

          secureTextEntry={true}
        />

        <TextInput
            placeholder="Enter Biodata"

            value={this.state.TextInput_Student_Bio}

            onChangeText={TextInputValue => this.setState({TextInput_Student_Bio : TextInputValue})}

            underlineColorAndroid='transparent'

            style={styles.TextAreaStyleClass}

            numberOfLines={10}
            
            multiline={true}
          />

        <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.UserUpdateFunction} >
            <Text style={styles.TextComponentStyle}> EDIT PROFILE </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
 
MainContainer :{
 
justifyContent: 'center',
flex:1,
margin: 10,
},
 
TextInputStyleClass: {
 
marginBottom: 7,
height: 40,
borderWidth: 1,
// Set border Hex Color Code Here.
 borderColor: '#2196F3',
 
 // Set border Radius.
 borderRadius: 5 ,
 
},

TextAreaStyleClass: {
 
marginBottom: 7,
height: 150,
borderWidth: 1,
// Set border Hex Color Code Here.
 borderColor: '#2196F3',
 
 // Set border Radius.
 borderRadius: 5 ,
 
},
 
 TextComponentStyle: {
   fontSize: 20,
  color: "#000",
  textAlign: 'center', 
  marginBottom: 15
 },

 TouchableOpacityStyle: {
   
    paddingTop:10,
    paddingBottom:10,
    borderRadius:5,
    marginBottom:7,
    width: '100%',
    backgroundColor: '#00BCD4'
 
  },
});
import React, { Component } from 'react';
import { 
  AsyncStorage,
  StyleSheet,
  Alert } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { 
  Container, 
  Header, 
  Left,
  Icon,
  Body,
  Title,
  Button,
  Content, 
  Form, 
  Text,
  Item, 
  Input,
  Textarea, 
  Thumbnail,
  Label } from 'native-base';

const options = {
  title: 'Select a photo',
  takePhotoButtonTitle: 'Take a photo',
  chooseFromLibraryButtonTitle: 'Choose from gallery',
  quality: 1
};

export default class FormStatus extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      feedContent: '',
      imageSource: null,
      data: null
    };

    AsyncStorage.getItem('id', (error, result) => {
        if (result) {
            this.setState({
                id: result
            });
        }
    });

    AsyncStorage.getItem('name', (error, result) => {
        if (result) {
            this.setState({
                name: result
            });
        }
    });
  }

  selectPhoto(){
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          imageSource: source,
          data: response.data
        });
      }
    });
  }

  UserPostFunction = () => {
    const { feedContent } = this.state;

    RNFetchBlob.fetch('POST', 'http://dev.bymankind.com/reactnative-api-crud/InsertStudentFeed.php', {
      Authorization : "Bearer access-token",
      otherHeader : "foo",
      'Content-Type' : 'multipart/form-data',
    }, [

      { name : 'image', filename : 'image.png', type : 'image/png', data : this.state.data },

      { name : 'feedData', data : JSON.stringify({
          student_id : this.state.id,
          feed_content : feedContent
      }) }

    ]).then((resp) => {
      if(resp['data'] == '"success"'){
        //redirect with oush for refresh to our destination
        this.props.navigation.navigate('Feeds');
      } else {
        Alert.alert(resp['data']);
      }
    }).catch((err) => {
      Alert.alert(error);
    })
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#44bba4' }}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back"/>
            </Button>
          </Left>
          <Body>
              <Title>Write Something Feed</Title>
          </Body>
        </Header>
        <Content>
          <Form>
            <Thumbnail square large style={styles.image} source={this.state.imageSource != null ? this.state.imageSource : require('../images/not_available2.jpg')} />
            <Textarea onChangeText={feedContent => this.setState({feedContent})} style={{ marginBottom: 7 }} rowSpan={5} bordered placeholder={ 'Hi, '+this.state.name+'. Write something here...' } />
            <Button style={{ backgroundColor: '#44bba4', marginBottom: 7 }} onPress={this.selectPhoto.bind(this)} full>
              <Text>Select Photo</Text>
            </Button>
            <Button style={{ backgroundColor: '#44bba4', marginBottom: 7 }} onPress={this.UserPostFunction} full>
              <Text>Post</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  image: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
  }

});
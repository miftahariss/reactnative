import React, { Component } from 'react';
import { 
  Image,
  AsyncStorage,
  RefreshControl,
  ActivityIndicator } from 'react-native';
import { 
  Container, 
  Header, 
  Title,
  Content, 
  Card, 
  CardItem, 
  Thumbnail, 
  Text, 
  Button, 
  Icon, 
  Left, 
  Right,
  Body,
  Fab,
  View } from 'native-base';
import TimeAgo from 'react-native-timeago';
//import firebase
import firebase from 'firebase';
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');
//import moment untuk mengubah bahasa indonesia "id"
import moment from 'moment';
import 'moment/locale/id';
moment.locale('id')
export default class Feeds extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
        dataFeed: [],
        dataLoadmore: [],
        page: 0, //for load more pagination
        isFetching: false, //for pull to refresh
        loading: true, //for loading before data load
        fetching_from_server: false, //for load more
        like: false,
        unlike: false,
        likeCount: 0
    };

    AsyncStorage.getItem('id', (error, result) => {
        if (result) {
            this.setState({
                id: result
            });
        }
    });

    AsyncStorage.getItem('photo', (error, result) => {
        if (result) {
            this.setState({
                photo: result
            });
        }
    });
  }

  componentWillMount(){
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyBzS4TAnv6HXW1w_uBZoI-ILHDWnkn1NN8",
      authDomain: "reactnativedatabase-3746f.firebaseapp.com",
      databaseURL: "https://reactnativedatabase-3746f.firebaseio.com",
      projectId: "reactnativedatabase-3746f",
      storageBucket: "reactnativedatabase-3746f.appspot.com",
      messagingSenderId: "128551177243"
    };
    //untuk mencegah error Firebase App named '[DEFAULT]' already exists
    if (!firebase.apps.length) {
        firebase.initializeApp(config);
    }
  }

  liked(feed_id){
    //create dynamic state
    const update = {};

    update['liked_' + feed_id] = true;

    this.setState(update);

    //insert firebase with random string key
    firebase.database().ref('likes/'+(Math.random()*1e32).toString(36)).set({
      feed_id: parseInt(feed_id),
      student_id: parseInt(this.state.id)
    });
  }

  // likeCount(feed_id){
  //   firebase.database().ref('likes/').on('value', function (snapshot) {
  //       console.log(snapshot.val().filter(like => like.feed_id === feed_id))
  //       this.setState({
  //          //dataSource: ds.cloneWithRows(responseJson),
  //          likeCount: snapshot.val().filter(like => like.feed_id === feed_id),
  //        }, function() {
  //          // In this block you can do something with new state.
  //        });
  //   }.bind(this));

  //   return this.state.likeCount;
  // }

  FormStatus(){
    this.props.navigation.push('FormStatus');
  }

  onRefresh = () => {
    this.setState({isFetching: true});
    this.getApiData().then(() => {
      this.setState({isFetching: false});
    });
  }

  getApiData() {
    return fetch('http://dev.bymankind.com/reactnative-api-crud/ShowAllFeedsList.php?page=0')
     .then((response) => response.json())
     .then((responseJson) => {
        //console.log(responseJson);
       //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
       this.setState({
         isFetching: false,
         //dataSource: ds.cloneWithRows(responseJson),
         dataFeed: responseJson,
       }, function() {
         // In this block you can do something with new state.
       });
     })
     .catch((error) => {
       console.error(error);
     });
  }

  loadMoreData = () => {
    this.state.page = this.state.page + 1;

    this.setState({ fetching_from_server: true }, () => {
        fetch('http://dev.bymankind.com/reactnative-api-crud/ShowAllFeedsList.php?page='+this.state.page)
         .then((response) => response.json())
         .then((responseJson) => {
            //console.log(responseJson);
           //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
           this.setState({
             isFetching: false,
             loading: false,
             fetching_from_server: false,
             //dataSource: ds.cloneWithRows(responseJson),
             dataFeed: this.state.dataFeed.concat(responseJson), //pake concat buat append array di loadmore
           }, function() {
             // In this block you can do something with new state.
           });
         })
         .catch((error) => {
           console.error(error);
         });
    })
  }

  componentDidMount() {

   firebase.database().ref('/likes/').on('value', function (snapshot) {
    console.log(snapshot.val());
   }.bind(this));
        
   return fetch('http://dev.bymankind.com/reactnative-api-crud/ShowAllFeedsList.php?page=0')
     .then((response) => response.json())
     .then((responseJson) => {
        //console.log(responseJson);
       //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
       this.setState({
         isFetching: false,
         loading: false,
         //dataSource: ds.cloneWithRows(responseJson),
         dataFeed: responseJson,
       }, function() {
         // In this block you can do something with new state.
       });
     })
     .catch((error) => {
       console.error(error);
     });
 }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#44bba4' }}>
          <Left>
            {/*<Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back"/>
            </Button>*/}
          </Left>
          <Body>
              <Title>Feeds</Title>
          </Body>
        </Header>
        <Content
          refreshControl={
            <RefreshControl
              refreshing={this.state.isFetching}
              onRefresh={this.onRefresh}
            />
          }
        >
          {
            ( this.state.loading )

            ?

            ( <ActivityIndicator size = "large" /> )

            :
            
            this.state.dataFeed.map((feed) => (

                <Card 
                  style={{flex: 0}}
                  
                >
                  <CardItem>
                    <Left>
                      <Thumbnail source={{uri: 'http://dev.bymankind.com/reactnative-api-crud/uploads/'+feed.student_photo}} />
                      <Body>
                        <Text>{feed.student_name}</Text>
                        <Text note>
                          {feed.student_class}
                        </Text>
                      </Body>
                    </Left>
                  </CardItem>
                  <CardItem>
                    <Body>
                      {feed.feed_image ? <Image source={{uri: 'http://dev.bymankind.com/reactnative-api-crud/uploads/'+feed.feed_image}} style={{height: 200, width: 345, flex: 1}}/> : <Text></Text>}
                      <Text>
                        {feed.feed_content}
                      </Text>
                    </Body>
                  </CardItem>
                  <CardItem>
                    <Left>
                      <Button transparent onPress={() => this.liked(feed.feed_id)}>
                        <Icon active name="thumbs-up" />
                        <Text>12 Likes</Text>
                      </Button>
                    </Left>
                    <Body>
                      <Button transparent>
                        <Icon active name="chatbubbles" />
                        <Text>4 Comments</Text>
                      </Button>
                    </Body>
                    <Right>
                      <Text><TimeAgo time={feed.feed_created_at} interval={20000} /></Text>
                    </Right>
                  </CardItem>
                </Card>

            ))
          }

          <Button style={{ backgroundColor: '#44bba4', marginBottom: 7 }} onPress={this.loadMoreData} full>
            <Text>Load More</Text>
            {
                ( this.state.fetching_from_server )
                ?
                    <ActivityIndicator color = "white" style = {{ marginLeft: 8 }} />
                :
                    null
            }
          </Button>

        </Content>

        <Fab
          direction="up"
          containerStyle={{ }}
          style={{ backgroundColor: '#44bba4' }}
          position="bottomRight"
          onPress={() => this.FormStatus()}
        >
          <Icon name="paper-plane" />
        </Fab>
      </Container>
    );
  }
}
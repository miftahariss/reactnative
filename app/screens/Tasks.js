/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, AsyncStorage, ScrollView, Alert, TouchableOpacity, ActivityIndicator} from 'react-native';
import Swipeable from 'react-native-swipeable';
import TimerCountdown from 'react-native-timer-countdown';
import firebase from 'firebase';
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

export default class Tasks extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      isFetching: false,
      dataTask: [],
      dataTaskProgress: [],
      toggle: false,
      leftActionActivated: false,
      isLoading: true,
    };

    AsyncStorage.multiGet(['id', 'name', 'class', 'phone', 'email', 'password']).then((data) => {
        let email = data[4][1];
        let password = data[5][1];

        if (email != null){
          //console.log('ada session');
        } else {
          //kalo ga ada session redirect ke login page/routes.js
          this.props.navigation.navigate('Login');
        }
    });

    AsyncStorage.getItem('email', (error, result) => {
        if (result) {
            this.setState({
                email: result
            });
        }
    });

    //for disable/hide the yellow box warning
    console.disableYellowBox = true;
  }

  componentWillMount(){
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyBzS4TAnv6HXW1w_uBZoI-ILHDWnkn1NN8",
      authDomain: "reactnativedatabase-3746f.firebaseapp.com",
      databaseURL: "https://reactnativedatabase-3746f.firebaseio.com",
      projectId: "reactnativedatabase-3746f",
      storageBucket: "reactnativedatabase-3746f.appspot.com",
      messagingSenderId: "128551177243"
    };
    firebase.initializeApp(config);

    const nameToSearch = 'unun';
    firebase.database().ref('tasks').once('value') //get all content from your node ref, it will return a promise
    .then(snapshot => { // then get the snapshot which contains an array of objects
      snapshot.val().filter(user => user.task_name === nameToSearch) // use ES6 filter method to return your array containing the values that match with the condition
    })

    //realtime database firebase
    // firebase.database().ref('tasks').on('value', (data) => {
    //   let ds = data.toJSON();
    //   console.log(data.val());
    // })

    // setTimeout(() => {
    //   firebase.database().ref('users/004').set({
    //     name: 'Mak Lampir',
    //     age: 40
    //   }).then(() => {
    //     console.log('INSERTED!');
    //   }).catch((error) => {
    //     console.log(error);
    //   });
    // }, 5000);

    // firebase.database().ref('users/004').update({
    //   name: 'Mak Lampir'
    // });

    //firebase.database().ref('users/004').remove();
  }

  onSwipe (task_id){
    //console.log(task_id)
    firebase.database().ref('tasks/'+task_id).update({
      task_status: 1
    });
  }

  secondsOnly(str){
      var p = str.split(':'),
          s = 0, m = 1;

      while (p.length > 0) {
          s += m * parseInt(p.pop(), 10);
          m *= 60;
      }

      return s;
  }

  backToRemaining(task_id){
    firebase.database().ref('tasks/'+task_id).update({
      task_status: 2
    });
  }

  taskTimeElapsed(name){
    Alert.alert('Task ' + name + ' sudah selesai!');
  }

  componentDidMount(){
    // fetch('http://dev.bymankind.com/reactnative-api-crud/ShowAllTask.php?status=2')
    //    .then((response) => response.json())
    //    .then((responseJson) => {
    //      //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    //      this.setState({
    //        isFetching: false,
    //        //dataTask: ds.cloneWithRows(responseJson),
    //        dataTask: responseJson,
    //      }, function() {
    //        // In this block you can do something with new state.
    //      });
    //    })
    //    .catch((error) => {
    //      console.error(error);
    //    });

    // firebase.database().ref('tasks').on('value', (data) => {
    //   let ds = data.toJSON();
    //   //console.log(data.val());
    //   this.setState({
    //      isFetching: false,
    //      //dataTask: ds.cloneWithRows(responseJson),
    //      dataTask: data.val(),
    //    }, function() {
    //      // In this block you can do something with new state.
    //    });
    // });

    // fetch('http://dev.bymankind.com/reactnative-api-crud/ShowAllTask.php?status=1')
    //    .then((response) => response.json())
    //    .then((responseJson) => {
    //      //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    //      this.setState({
    //        isFetching: false,
    //        //dataSource: ds.cloneWithRows(responseJson),
    //        dataTaskProgress: responseJson,
    //      }, function() {
    //        // In this block you can do something with new state.
    //      });
    //    })
    //    .catch((error) => {
    //      console.error(error);
    //    });

    const status_progress = 2;
    firebase.database().ref('tasks/').on('value', function (snapshot) {
        //console.log(snapshot.val().filter(user => user.task_status === status_progress))
        this.setState({
           isFetching: false,
           isLoading: false,
           //dataSource: ds.cloneWithRows(responseJson),
           dataTask: snapshot.val().filter(user => user.task_status === status_progress),
         }, function() {
           // In this block you can do something with new state.
         });
    }.bind(this));

    const status_done = 1;
    firebase.database().ref('tasks/').on('value', function (snapshot) {
        //console.log(snapshot.val().filter(user => user.task_status === status_progress))
        this.setState({
           isFetching: false,
           //dataSource: ds.cloneWithRows(responseJson),
           dataTaskProgress: snapshot.val().filter(user => user.task_status === status_done),
         }, function() {
           // In this block you can do something with new state.
         });
    }.bind(this));
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }

    return (

      <View style={styles2.container}>
        <ScrollView>
        <Text style= {{
              fontSize: 20,
              color: "#000",
              textAlign: 'center', 
              marginBottom: 15
            }}>Tasks Remaining</Text>
        {
          this.state.dataTask.map((userData) => (
            <View style = { styles2.rowViewContainer }>
              <Swipeable
                leftActionActivationDistance={200}
                leftContent={(
                  <View>
                    {this.state.leftActionActivated ?
                      <Text></Text> :
                      <Text></Text>}
                  </View>
                )}
                onLeftActionActivate={() => this.setState({leftActionActivated: true})}
                onLeftActionDeactivate={() => this.setState({leftActionActivated: false})}
                // onLeftActionComplete={() => this.setState({toggle: !this.state.toggle})}
                onLeftActionComplete={() => this.onSwipe(userData.task_id)}
              >
                <View>
                  <Text style={ styles2.welcome }>{ userData.task_name + ' ( ' + userData.task_time + ' )' }</Text>
                </View>
              </Swipeable>
              <View
                 style={{
                   height: .5,
                   width: "100%",
                   backgroundColor: "#000",
                 }}
               />
            </View>
          ))
        }
        </ScrollView>

        <ScrollView>
        <Text style= {{
                fontSize: 20,
                color: "#000",
                textAlign: 'center', 
                marginBottom: 15
              }}>Tasks Progress</Text>
        {
          this.state.dataTaskProgress.map((userData) => (
            <View style = { styles2.rowViewContainer }>
                <Text style = { styles2.welcome }>{ userData.task_name + ' ( ' + userData.task_time + ' )' }</Text>
                <TouchableOpacity activeOpacity = { .4 } style={styles2.TouchableOpacityStyle} onPress={() => this.backToRemaining(userData.task_id)} >
                    <Text> Back </Text>
                </TouchableOpacity>
                <TimerCountdown
                      initialSecondsRemaining={this.secondsOnly(userData.task_time)*1000}
                      onTimeElapsed={() => this.taskTimeElapsed(userData.task_name)}
                      allowFontScaling={true}
                      style={{ fontSize: 20 }}
                  />
                <View
                   style={{
                     height: .5,
                     width: "100%",
                     backgroundColor: "#000",
                   }}
                 />
            </View>
          ))
        }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const addItemStyles = StyleSheet.create({
    wrapper: {
        padding: 10,
        backgroundColor: '#FFFFFF'
    },


});

const styles2 = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
    backgroundColor: '#F5FCFF',
    alignItems: 'flex-start',
    flexDirection:"row"
  },
  welcome: {
    flex: 1,
    margin: 20,
    margin: 10,
  },
  rowViewContainer: {
    fontSize: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  TouchableOpacityStyle: {
   
    paddingTop:10,
    paddingBottom:10,
    borderRadius:5,
    marginBottom:7,
    width: '100%',
    backgroundColor: '#00BCD4'
 
  },
});


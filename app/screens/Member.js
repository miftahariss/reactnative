/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, AsyncStorage, ScrollView, TouchableOpacity, Image, FlatList} from 'react-native';
import { SearchBar } from 'react-native-elements';
import TimeAgo from 'react-native-timeago';
//import moment untuk mengubah bahasa indonesia "id"
import moment from 'moment';
import 'moment/locale/id';
moment.locale('id')

export default class Member extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      // data:[
      //   {id:1, image: "https://bootdey.com/img/Content/avatar/avatar1.png", name:"Frank Odalthh",    comment:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."},
      //   {id:2, image: "https://bootdey.com/img/Content/avatar/avatar6.png", name:"John DoeLink",     comment:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."},
      //   {id:3, image: "https://bootdey.com/img/Content/avatar/avatar7.png", name:"March SoulLaComa", comment:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."},
      //   {id:4, image: "https://bootdey.com/img/Content/avatar/avatar2.png", name:"Finn DoRemiFaso",  comment:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."},
      //   {id:5, image: "https://bootdey.com/img/Content/avatar/avatar3.png", name:"Maria More More",  comment:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."},
      //   {id:6, image: "https://bootdey.com/img/Content/avatar/avatar4.png", name:"Clark June Boom!", comment:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."},
      //   {id:7, image: "https://bootdey.com/img/Content/avatar/avatar5.png", name:"The googler",      comment:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor."},
      // ]
      isFetching: false,
    }

    //bikin default array untuk result pencarian
    this.arrayholder = [];

    AsyncStorage.multiGet(['id', 'name', 'class', 'phone', 'email', 'password']).then((data) => {
        let email = data[4][1];
        let password = data[5][1];

        if (email != null){
          //console.log('ada session');
        } else {
          //kalo ga ada session redirect ke login page/routes.js
          this.props.navigation.navigate('Login');
        }
    });

    AsyncStorage.getItem('email', (error, result) => {
        if (result) {
            this.setState({
                email: result
            });
        }
    });

    //for disable/hide the yellow box warning
    console.disableYellowBox = true;
  }

  onRefresh() {
     this.setState({ isFetching: true }, function() { this.getApiData() });
  }

  getApiData() {
    return fetch('http://dev.bymankind.com/reactnative-api-crud/ShowAllStudentsList.php')
       .then((response) => response.json())
       .then((responseJson) => {
         //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
         this.setState({
           isFetching: false,
           //dataSource: ds.cloneWithRows(responseJson),
           data: responseJson,
         }, function() {
           // In this block you can do something with new state.
         });
       })
       .catch((error) => {
         console.error(error);
       });
  }

  searchFilterFunction = (text) => {
    //console.log(this.arrayholder);
    //menggunakan data dari this.arrayholder yang dibuat ketika fetch data
    //dan menggunakan function filter untuk mencari
    const newData = this.arrayholder.filter(item => {
      const itemData = 
        `${item.student_name.toUpperCase()} 
        ${item.studnet_phone_number} 
        ${item.student_email} 
        ${item.student_bio}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    //console.log(newData);
    //replace state data dengan data yang baru yang berisi hasil pencarian
    this.setState({
      data: newData
    });
  };

  renderHeader = () => {
    //render header search bar
    return (
      <SearchBar
        placeholder="Cari.."
        lightTheme
        round
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
      />
    );
  }

  componentDidMount() {
        
     return fetch('http://dev.bymankind.com/reactnative-api-crud/ShowAllStudentsList.php')
       .then((response) => response.json())
       .then((responseJson) => {
         //let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
         this.setState({
           isFetching: false,
           //dataSource: ds.cloneWithRows(responseJson),
           data: responseJson,
         }, function() {
           // In this block you can do something with new state.
         });
         this.arrayholder = responseJson;
       })
       .catch((error) => {
         console.error(error);
       });
   }

  render() {
    return (
      <FlatList
        ListHeaderComponent={this.renderHeader}
        style={styles.root}
        data={this.state.data}
        onRefresh={() => this.onRefresh()}
        refreshing={this.state.isFetching}
        extraData={this.state}
        ItemSeparatorComponent={() => {
          return (
            <View style={styles.separator}/>
          )
        }}
        keyExtractor={(item)=>{
          return item.student_id;
        }}
        renderItem={(item) => {
          const Notification = item.item;
          return(
            <View style={styles.container}>
              <TouchableOpacity onPress={() => {}}>
                <Image style={styles.image} source={{uri: 'http://dev.bymankind.com/reactnative-api-crud/uploads/'+Notification.student_photo}}/>
              </TouchableOpacity>
              <View style={styles.content}>
                <View style={styles.contentHeader}>
                  <Text  style={styles.name}>{Notification.student_name}</Text>
                  <Text style={styles.time}>
                    {/* interval param would update every 20 seconds */}
                    <TimeAgo time={Notification.student_created_at} interval={20000} />
                  </Text>
                </View>
                <Text rkType='primary3 mediumLine'>{Notification.student_bio}</Text>
              </View>
            </View>
          );
        }}/>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: "#ffffff",
    marginTop:10,
  },
  container: {
    paddingLeft: 19,
    paddingRight: 16,
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  content: {
    marginLeft: 16,
    flex: 1,
  },
  contentHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 6
  },
  separator: {
    height: 1,
    backgroundColor: "#CCCCCC"
  },
  image:{
    width:45,
    height:45,
    borderRadius:20,
    marginLeft:20
  },
  time:{
    fontSize:11,
    color:"#808080",
  },
  name:{
    fontSize:16,
    fontWeight:"bold",
  },
});  

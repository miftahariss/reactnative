import React, {Component} from 'react';
import { StyleSheet, TextInput, View, Alert, TouchableOpacity, Text, AsyncStorage, Image, ScrollView } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';

const options = {
  title: 'Select a photo',
  takePhotoButtonTitle: 'Take a photo',
  chooseFromLibraryButtonTitle: 'Choose from gallery',
  quality: 1
};

export default class Register extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      userName: '',
      userClass: '',
      userPhoneNumber: '',
      userEmail: '',
      userPassword: '',
      userBio: '',
      imageSource: null,
      data: null
    };

    AsyncStorage.multiGet(['email', 'password']).then((data) => {
        let email = data[0][1];
        let password = data[1][1];

        if (email != null){
          this.props.navigation.navigate('Dashboard');
        }
    });

    //for disable/hide the yellow box warning
    console.disableYellowBox = true;
  }

  //bikin function post register
  UserRegisterFunction = () => {
    const { userName } = this.state;
    const { userClass } = this.state;
    const { userPhoneNumber } = this.state;
    const { userEmail } = this.state;
    const { userPassword } = this.state;
    const { userBio } = this.state;

    RNFetchBlob.fetch('POST', 'http://dev.bymankind.com/reactnative-api-crud/InsertStudentData.php', {
      Authorization : "Bearer access-token",
      otherHeader : "foo",
      'Content-Type' : 'multipart/form-data',
    }, [
      
      { name : 'image', filename : 'image.png', type:'image/png', data: this.state.data},

      { name : 'userData', data : JSON.stringify({
            student_name : userName,
            student_class : userClass,
            student_phone_number : userPhoneNumber,
            student_email : userEmail,
            student_password : userPassword,
            student_bio : userBio
      }) }
      
    ]).then((resp) => {
      if(resp['data'] == '"success"'){
        //console.log(resp['data'], 'success coy');
        Alert.alert('Thanks for register. please login with your email and password.');

        //redirect
        this.props.navigation.navigate('Login');
      } else {
        //console.log(resp);
        Alert.alert(resp['data']);
      }
    }).catch((err) => {
      Alert.alert(error);
    })

    // fetch('http://dev.bymankind.com/reactnative-api-crud/InsertStudentData.php', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify({
    //     //variable yang akan dibawa ke api
    //     student_name : userName,
    //     student_class : userClass,
    //     student_phone_number : userPhoneNumber,
    //     student_email : userEmail,
    //     student_password : userPassword
    //   })
    // }).then((response) => response.json())
    //     .then((responseJson) => {
    //       if(responseJson == 'success'){
    //         Alert.alert('Thanks for register. please login with your email and password.');

    //         //redirect
    //         this.props.navigation.navigate('Login');
    //       } else {
    //         Alert.alert('Oops! Something went wrong.')
    //       }
    //     }).catch((error) => {
    //       Alert.alert(error);
    //     });
  }

  selectPhoto(){
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          imageSource: source,
          data: response.data
        });
      }
    });
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.MainContainer}>
          <Text style={ styles.TextComponentStyle }>Registration</Text>

          <TextInput
            placeholder="Enter User Name"

            onChangeText={userName => this.setState({userName})}

            underlineColorAndroid='transparent'

            style={styles.TextInputStyleClass}
          />

          <TextInput
            placeholder="Enter User Class"

            onChangeText={userClass => this.setState({userClass})}

            underlineColorAndroid='transparent'

            style={styles.TextInputStyleClass}
          />

          <TextInput
            placeholder="Enter User Phone Number"

            onChangeText={userPhoneNumber => this.setState({userPhoneNumber})}

            underlineColorAndroid='transparent'

            style={styles.TextInputStyleClass}
          />

          <TextInput
            placeholder="Enter User Email"

            onChangeText={userEmail => this.setState({userEmail})}

            underlineColorAndroid='transparent'

            style={styles.TextInputStyleClass}
          />

          <TextInput
            placeholder="Enter User Password"

            onChangeText={userPassword => this.setState({userPassword})}

            underlineColorAndroid='transparent'

            style={styles.TextInputStyleClass}

            secureTextEntry={true}
          />

          <TextInput
            placeholder="Enter Biodata"

            onChangeText={userBio => this.setState({userBio})}

            underlineColorAndroid='transparent'

            style={styles.TextAreaStyleClass}

            numberOfLines={10}
            
            multiline={true}
          />

          <Image style={styles.image}
                source={this.state.imageSource != null ? this.state.imageSource : require('../images/not_available.jpg')}
          />

          <TouchableOpacity style={styles.buttonUpload} onPress={this.selectPhoto.bind(this)}>
            <Text style={styles.TextComponentStyle}>Select Photo Profile</Text>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.UserRegisterFunction} >
              <Text style={styles.TextComponentStyle}> REGISTER </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
 
MainContainer :{
 
justifyContent: 'center',
flex:1,
margin: 10,
},
 
TextInputStyleClass: {
 
marginBottom: 7,
height: 40,
borderWidth: 1,
// Set border Hex Color Code Here.
 borderColor: '#2196F3',
 
 // Set border Radius.
 borderRadius: 5 ,
 
},

TextAreaStyleClass: {
 
marginBottom: 7,
height: 150,
borderWidth: 1,
// Set border Hex Color Code Here.
 borderColor: '#2196F3',
 
 // Set border Radius.
 borderRadius: 5 ,
 
},
 
 TextComponentStyle: {
   fontSize: 20,
  color: "#000",
  textAlign: 'center', 
  marginBottom: 15
 },

 TouchableOpacityStyle: {
   
    paddingTop:10,
    paddingBottom:10,
    borderRadius:5,
    marginBottom:7,
    width: '100%',
    backgroundColor: '#00BCD4'
 
  },

  buttonUpload: {
     paddingTop:10,
    paddingBottom:10,
    borderRadius:5,
    marginBottom:7,
    width: '100%',
    backgroundColor: '#330066',
    borderRadius: 30,

  },
  image: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
  }
});

import React, { Component } from 'react';
import { 
	View ,
	AsyncStorage,
	ActivityIndicator,
	Alert
} from "react-native";
import {
	Container,
	Header,
	Title,
	Button,
	IconNB,
	DeckSwiper,
	SwipeRow,
    Card,
    CardItem,
    Icon,
	Left,
	Right,
	Body,
	Content,
	List,
	ListItem,
	Text
} from 'native-base';
import { Grid, Col } from 'react-native-easy-grid';
import TimerCountdown from 'react-native-timer-countdown';
import firebase from 'firebase';
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

export default class TasksNew extends Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	  isFetching: false,
	      dataTask: [],
	      dataTaskProgress: [],
	      toggle: false,
	      leftActionActivated: false,
	      isLoading: true,
	  };

	  AsyncStorage.multiGet(['id', 'name', 'class', 'phone', 'email', 'password']).then((data) => {
	        let email = data[4][1];
	        let password = data[5][1];

	        if (email != null){
	          //console.log('ada session');
	        } else {
	          //kalo ga ada session redirect ke login page/routes.js
	          this.props.navigation.navigate('Login');
	        }
	    });

	    AsyncStorage.getItem('email', (error, result) => {
	        if (result) {
	            this.setState({
	                email: result
	            });
	        }
	    });

	    //for disable/hide the yellow box warning
	    console.disableYellowBox = true;
	}

	componentWillMount(){
	    // Initialize Firebase
	    var config = {
	      apiKey: "AIzaSyBzS4TAnv6HXW1w_uBZoI-ILHDWnkn1NN8",
	      authDomain: "reactnativedatabase-3746f.firebaseapp.com",
	      databaseURL: "https://reactnativedatabase-3746f.firebaseio.com",
	      projectId: "reactnativedatabase-3746f",
	      storageBucket: "reactnativedatabase-3746f.appspot.com",
	      messagingSenderId: "128551177243"
	    };
	    //untuk mencegah error Firebase App named '[DEFAULT]' already exists
	    if (!firebase.apps.length) {
		    firebase.initializeApp(config);
		}

	    const nameToSearch = 'unun';
	    firebase.database().ref('tasks').once('value') //get all content from your node ref, it will return a promise
	    .then(snapshot => { // then get the snapshot which contains an array of objects
	      snapshot.val().filter(user => user.task_name === nameToSearch) // use ES6 filter method to return your array containing the values that match with the condition
	    })
	  }

	onSwipe (task_id, task_time){
	    //console.log(task_id);
	    //AsyncStorage.setItem('time_'+task_id, task_time)

	    firebase.database().ref('tasks/'+task_id).update({
	      task_status: 1
	    });

	    const update = {};

	    update['timeAwal_' + task_id] = Math.round(+new Date()/1000);
	    update['remainingTimes_' + task_id] = this.secondsOnly(task_time)*1000;

	    this.setState(update);

	    //AsyncStorage.setItem('timeAwal_'+task_id, JSON.stringify(Math.round(+new Date()/1000)));
	    //AsyncStorage.setItem('remainingTimes_'+task_id, JSON.stringify(this.secondsOnly(task_time)*1000));

	    // if(!this.state.timeAwal_+task_id){
	    // 	//create dynamic state
		   //  const update = {};

		   //  update['timeAwal_' + task_id] = Math.round(+new Date()/1000);
		   //  update['remainingTimes_' + task_id] = this.secondsOnly(task_time)*1000;

		   //  this.setState(update);
	    // } else {
	    // 	const times = Math.round(+new Date()/1000) - this.state.timeAwal_+task_id;
	    // 	const time_ = this.secondsOnly(task_time)*1000 - times;
	    // 	//create dynamic state
		   //  const update = {};
		   //  update['remainingTimes_' + task_id] = time_;

		   //  this.setState(update);
	    // }
	  }

	tickTime(task_id, secondsRemaining){
		//console.log('tick_'+task_id);
		//AsyncStorage.setItem('tick_'+task_id, secondsRemaining)
	}

	secondsOnly(str){
	      var p = str.split(':'),
	          s = 0, m = 1;

	      while (p.length > 0) {
	          s += m * parseInt(p.pop(), 10);
	          m *= 60;
	      }

	      return s;
	  }

	backToRemaining(task_id){
	    firebase.database().ref('tasks/'+task_id).update({
	      task_status: 2
	    });

	    //create dynamic state
	    const update = {};
	    update['timeAwal_' + task_id] = false;
	    update['remainingTimes_' + task_id] = false;
	    this.setState(update);
	  }

	taskTimeElapsed(task_id, task_name){
		const update = {};
	    update['timeAwal_' + task_id] = false;
	    update['remainingTimes_' + task_id] = false;
	    this.setState(update);

	    Alert.alert('Task ' + task_name + ' sudah selesai!');
	  }

	componentDidMount(){
		const status_progress = 2;
	    firebase.database().ref('tasks/').on('value', function (snapshot) {
	        //console.log(snapshot.val().filter(user => user.task_status === status_progress))
	        this.setState({
	           isFetching: false,
	           isLoading: false,
	           //dataSource: ds.cloneWithRows(responseJson),
	           dataTask: snapshot.val().filter(user => user.task_status === status_progress),
	         }, function() {
	           // In this block you can do something with new state.
	         });
	    }.bind(this));

	    const status_done = 1;
	    firebase.database().ref('tasks/').on('value', function (snapshot) {
	        //console.log(snapshot.val().filter(user => user.task_status === status_progress))
	        this.setState({
	           isFetching: false,
	           //dataSource: ds.cloneWithRows(responseJson),
	           dataTaskProgress: snapshot.val().filter(user => user.task_status === status_done),
	         }, function() {
	           // In this block you can do something with new state.
	         });

	        snapshot.val().filter(user => user.task_status === status_done).map((data) => {

	        	if(this.state['timeAwal_'+data.task_id]){
			    	const times = Math.round(+new Date()/1000) - this.state['timeAwal_'+data.task_id];
			    	const time_ = this.secondsOnly(data.task_time) - times;

			    	//create dynamic state
				    const update = {};
				    update['remainingTimes_' + data.task_id] = time_*1000;

				    this.setState(update);
			    }
	        })
	    }.bind(this));
	}

	render() {
		if (this.state.isLoading) {
	      return (
	        <View style={{flex: 1, paddingTop: 20}}>
	          <ActivityIndicator />
	        </View>
	      );
	    }

		return (
			<Container>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back"/>
						</Button>
					</Left>
					<Body>
			            <Title>Tasks</Title>
			        </Body>
				</Header>
				<Grid>
		          <Col style={{ backgroundColor: "#635DB7" }}>
		          	<Content scrollEnabled={true}>
		          	{
		          		this.state.dataTask.map((userData) => (
		          			<SwipeRow
					            leftOpenValue={75}
					            rightOpenValue={-75}
					            left={
					              <Button success onPress={() => this.onSwipe(userData.task_id, userData.task_time)}>
					                <Icon active name="add" />
					              </Button>
					            }
					            disableLeftSwipe={true}
					            body={
					              <View style={{ paddingLeft: 20 }}>
					                <Text>{ userData.task_name + ' ( ' + userData.task_time + ' )' }</Text>
					              </View>
					            }
					          />
		          		))
		          	}
			        </Content>
		          </Col>
		          <Col style={{ backgroundColor: "#00CE9F" }}>
		          	<Content scrollEnabled={true}>
		          	{
		          		this.state.dataTaskProgress.map((userData) => (
		          			<SwipeRow
					            leftOpenValue={75}
					            rightOpenValue={-75}
					            right={
					              <Button danger onPress={() => this.backToRemaining(userData.task_id)}>
					                <Icon active name="trash" />
					              </Button>
					            }
					            disableRightSwipe={true}
					            body={
					              <View style={{ paddingLeft: 20 }}>
					                <Text>{ userData.task_name + ' ( ' + userData.task_time + ' )' }</Text>
					              	<TimerCountdown
					              	  //onTick={secondsRemaining => console.log('tick', secondsRemaining)}
					              	  onTick={secondsRemaining => this.tickTime(userData.task_id, secondsRemaining)}
				                      //initialSecondsRemaining={this.state.remainingTimes_+userData.task_id}
				                      initialSecondsRemaining={this.state['remainingTimes_'+userData.task_id]}
				                      onTimeElapsed={() => this.taskTimeElapsed(userData.task_id, userData.task_name)}
				                      allowFontScaling={true}
				                      style={{ fontSize: 20 }}
				                  />
					              </View>
					            }
					          />
		          		))
		          	}
			        </Content>
		          </Col>
		        </Grid>
			</Container>
		);
	}
}
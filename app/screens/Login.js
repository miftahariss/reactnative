import React, {Component} from 'react';
import { StyleSheet, TextInput, View, Alert, TouchableOpacity, Text, AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation';

export default class Login extends Component {
    constructor(props) {
      super(props);
    
      this.state = {
        userEmail: '',
        userPassword: ''
      };

      AsyncStorage.multiGet(['email']).then((data) => {
          //urutan array tergantung di dalem param multiget
          let email = data[0][1];
          console.log(email);

          if (email != null){
            this.props.navigation.navigate('Dashboard');
          }
      });
    }

    //bikin function post login
    UserLoginFunction = () => {
      const { userEmail } = this.state;
      const { userPassword } = this.state;

      fetch('http://dev.bymankind.com/reactnative-api-crud/Login.php', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          //variable yang akan dibawa ke API
          email: userEmail,
          password: userPassword
        })
      }).then((response) => response.json())
        .then((responseJson) => {
          //jika datanya ada
          //console.log(responseJson);
          if(responseJson != '404'){

            //Then open Profile activity and send user email to profile activity.
            //this.props.navigation.navigate('Second', { Email: UserEmail });
            //Alert.alert(responseJson);
            Alert.alert('Anda berhasil Login.');
            //save email dan password di session
            AsyncStorage.multiSet([
                ["id", responseJson['student_id']],
                ["name", responseJson['student_name']],
                ["class", responseJson['student_class']],
                ["phone", responseJson['student_phone_number']],
                ["email", userEmail],
                ["password", userPassword],
                ["photo", responseJson['student_photo']],
                ["bio", responseJson['student_bio']]
            ]);

            //redirect
            this.props.navigation.navigate('Dashboard', { Email: userEmail });
          } else {
            //Alert.alert(responseJson);
            Alert.alert('Email atau Password anda salah.')
          }
        }).catch((error) => {
          console.error(error);
        });
    }

    render(){
      return(
        <View style={styles.MainContainer}>
 
                <Text style= {styles.TextComponentStyle}>Login</Text>
          
                <TextInput
                  
                  // Adding hint in Text Input using Place holder.
                  placeholder="Enter User Email"
         
                  onChangeText={userEmail => this.setState({userEmail})}
         
                  // Making the Under line Transparent.
                  underlineColorAndroid='transparent'
         
                  style={styles.TextInputStyleClass}
                />
         
                <TextInput
                  
                  // Adding hint in Text Input using Place holder.
                  placeholder="Enter User Password"
         
                  onChangeText={userPassword => this.setState({userPassword})}
         
                  // Making the Under line Transparent.
                  underlineColorAndroid='transparent'
         
                  style={styles.TextInputStyleClass}
         
                  secureTextEntry={true}
                />
                {/* Button sudah tidak bisa digunakan di react native versi terbaru */}
                {/* <Button title="Click Here To Login" onPress={this.UserLoginFunction} color="#2196F3" /> */}
                <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.UserLoginFunction} >
                    <Text style={styles.TextComponentStyle}> LOGIN </Text>
                </TouchableOpacity>
        </View>
      );
    }
}

const styles = StyleSheet.create({
 
MainContainer :{
 
justifyContent: 'center',
flex:1,
margin: 10,
},
 
TextInputStyleClass: {
 
marginBottom: 7,
height: 40,
borderWidth: 1,
// Set border Hex Color Code Here.
 borderColor: '#2196F3',
 
 // Set border Radius.
 borderRadius: 5 ,
 
},
 
 TextComponentStyle: {
   fontSize: 20,
  color: "#000",
  textAlign: 'center', 
  marginBottom: 15
 },

 TouchableOpacityStyle: {
   
    paddingTop:10,
    paddingBottom:10,
    borderRadius:5,
    marginBottom:7,
    width: '100%',
    backgroundColor: '#00BCD4'
 
  },
});



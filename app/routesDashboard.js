import React, { Component } from 'react';
import { Dimensions, Platform, View, Text } from 'react-native';
import { StackNavigator, createStackNavigator, createBottomTabNavigator, withNavigation } from 'react-navigation';
import { Icon } from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Tasks from './screens/Tasks';
import TasksNew from './screens/TasksNew';
import Feeds from './screens/Feeds';
import Member from './screens/Member';
import Profile from './screens/Profile';
import EditProfile from './screens/EditProfile';
import FormStatus from './screens/FormStatus';

import Login from './routes';

let screen = Dimensions.get('window');

export const Tabs = createBottomTabNavigator({
	'Tasks': {
		screen: Tasks,
		navigationOptions: {
			tabBarLabel: 'Tasks',
			tabBarIcon: ({ tintColor }) => <Icon name="format-list-bulleted" type="MaterialCommunityIcons" size={28} color="#44bba4" />
		}
	},
	'TasksNew': {
		screen: TasksNew,
		navigationOptions: {
			tabBarLabel: 'TasksNew',
			tabBarIcon: ({ tintColor }) => <Icon name="format-list-bulleted" type="MaterialCommunityIcons" size={28} color="#44bba4" />
		}
	},
	'Feeds': {
		screen: Feeds,
		navigationOptions: {
			tabBarLabel: 'Feeds',
			tabBarIcon: ({ tintColor }) => <FontAwesome name="feed" size={28} color="#44bba4" />
		}
	},
	'Member': {
		screen: Member,
		navigationOptions: {
			tabBarLabel: 'Member',
			tabBarIcon: ({ tintColor }) => <FontAwesome name="users" size={28} color="#44bba4" />
		}
	},
	'My Profile': {
		screen: Profile,
		navigationOptions: {
			tabBarLabel: 'My Profile',
			tabBarIcon: ({ tintColor }) => <Icon name="ios-person" type="ionicon" size={28} color="#44bba4" />
		}
	}
});

// export const ProductStack = createStackNavigator({
// 	Tasks: {
// 		screen: Tasks,
// 		navigationOptions: ({navigation}) => ({
// 			header: null
// 		})
// 	}
// });

export const Dashboard = () => {
	return createStackNavigator(
		{
			Tabs: {
				screen: Tabs,
				navigationOptions: ({navigation}) => ({
					gesturesEnabled: false
				})
			},
			Tasks: {
				screen: Tasks,
				navigationOptions: ({navigation}) => ({
					gesturesEnabled: false
				})
			},
			'Login': {
				screen: Login,
				navigationOptions: ({navigation}) => ({
					gesturesEnabled: false
				})
			}
		},
		{
			headerMode: "none",
			mode: "modal"
		}
	);
};

export default createStackNavigator(
	{
		Tabs: {
			screen: Tabs,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		},
		Tasks: {
			screen: Tasks,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		},
		'Login': {
			screen: Login,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		},
		'EditProfile': {
			screen: EditProfile,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		},
		'Feeds': {
			screen: Feeds,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		},
		'FormStatus': {
			screen: FormStatus,
			navigationOptions: ({navigation}) => ({
				gesturesEnabled: false
			})
		}
	},
	{
		headerMode: "none",
		mode: "modal"
	}
);